using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BalasUI : MonoBehaviour
{
    public Disparador disparador;

    public TextMeshProUGUI textoBalas;

    // Update is called once per frame
    void Update()
    {
        textoBalas = GetComponent<TextMeshProUGUI>();

        textoBalas.text = disparador.currentAmmo.ToString() + "/" + disparador.maxAmmo.ToString();
    }
}
