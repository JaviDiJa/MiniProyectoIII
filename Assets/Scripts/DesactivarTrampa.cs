using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesactivarTrampa : MonoBehaviour
{
    public BloqueoPuertas bloqueoPuertas;

    [SerializeField] private GameObject fuegos;

    public bool fuegosApagados;

    void Start()
    {
        fuegosApagados = false;
        fuegos.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (bloqueoPuertas.puertaDesbloqueada)
        {
            fuegos.SetActive(false);
            fuegosApagados = true;
        }
    }
}
