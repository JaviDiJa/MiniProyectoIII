using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoRecovery : MonoBehaviour
{
    [SerializeField] private int ammoRecovery;

    public Disparador disparador;

    public ParticleSystem ammoPrefab;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && disparador.maxAmmo != disparador.totalAmmo)
        {
            disparador.totalAmmo = disparador.totalAmmo + ammoRecovery;

            ParticleSystem vida = Instantiate(ammoPrefab, transform.position, Quaternion.identity);
            Invoke("Recoger", 0.25f);
        }
    }

    void Recoger()
    {
        Destroy(gameObject);
    }
}
