using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TotalBalas : MonoBehaviour
{
    public Disparador disparador;

    public TextMeshProUGUI textoBalas;

    // Update is called once per frame
    void Update()
    {
        textoBalas = GetComponent<TextMeshProUGUI>();

        textoBalas.text = disparador.totalAmmo.ToString();
    }
}
