using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionPrefab : MonoBehaviour
{
    public ParticleSystem explosionFx;

    private void OnDestroy()
    {
        Instantiate(explosionFx, transform.position, Quaternion.identity);

        DestroyImmediate(explosionFx);
    }
}
