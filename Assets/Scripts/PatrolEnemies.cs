using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolEnemies : MonoBehaviour
{
    [SerializeField] private Transform[] posicionPatrulla;
    //[SerializeField] private Transform jugador;

    //[SerializeField] private float rangoDeteccion;
    [SerializeField] private float tiempoVigilancia;
 
    private GameObject jugador;

    private float tiempoRestanteVigilancia;

    private bool vigilando;

    private NavMeshAgent enemigo;
    private Animator animator;

    private Vector3 ultimaPosicionConocida;

    private int indicePosicionPatrulla;

    public EnemyExplosion enemyExplosion;

    public enum EstadoEnemigo
    {
        Patrullando,
        Vigilando,
        Persiguiendo
    }

    public EstadoEnemigo estadoActual;

    // Start is called before the first frame update
    void Awake()
    {
        enemigo = GetComponent<NavMeshAgent>();

        animator = GetComponent<Animator>();

        jugador = GameObject.FindGameObjectWithTag("Player");

        ElegirDestinoAleatorio();
    }

    private void Start()
    {
        Patrullando();

        ultimaPosicionConocida = jugador.transform.position;

        tiempoRestanteVigilancia = tiempoVigilancia;
    }

    // Update is called once per frame
    void Update()
    {
        switch (estadoActual)
        {
            case EstadoEnemigo.Patrullando:

                Patrullando();

                animator.SetBool("isPersiguiendo", true);

                break;

            case EstadoEnemigo.Persiguiendo:

                Persiguiendo();

                animator.SetBool("isPersiguiendo", true);

                break;

            case EstadoEnemigo.Vigilando:

                Vigilando();

                animator.SetBool("isPersiguiendo", false);

                break;
        }

        RecibirDaņo();

        //if (Physics.Raycast(transform.position, jugador.transform.position - transform.position, out RaycastHit hit, rangoDeteccion))
        //{
        //    if (hit.collider.CompareTag("Player"))
        //    {
        //        estadoAcual = EstadoEnemigo.Persiguiendo;
        //    }
        //}
    }

    private void OnTriggerEnter(Collider other)
    {
        Persiguiendo();
    }

    private void OnTriggerExit(Collider other)
    {
        vigilando = true;

        Vigilando();
    }

    void RecibirDaņo()
    {
        if (enemyExplosion.currentEnemyHealth != enemyExplosion.maxEnemyHealth)
        {
            Persiguiendo();
        }
    }

    void Patrullando()
    {
        estadoActual = EstadoEnemigo.Patrullando;

        if (!vigilando && Vector3.Distance(enemigo.transform.position, posicionPatrulla[indicePosicionPatrulla].position) < 1.5f)
        {
            indicePosicionPatrulla++;

            if (indicePosicionPatrulla >= posicionPatrulla.Length)
            {
                indicePosicionPatrulla = 0;
            }

            enemigo.SetDestination(posicionPatrulla[indicePosicionPatrulla].position);
        }
    }

    void Persiguiendo()
    {
        estadoActual = EstadoEnemigo.Persiguiendo;

        ultimaPosicionConocida = jugador.transform.position;

        enemigo.SetDestination(jugador.transform.position);
    }

    void Vigilando()
    {
        estadoActual = EstadoEnemigo.Vigilando;

        enemigo.SetDestination(ultimaPosicionConocida);

        if (tiempoRestanteVigilancia > 0)
        {
            tiempoRestanteVigilancia -= Time.deltaTime;

            vigilando = true;
        }
        else if (tiempoRestanteVigilancia <= 0)
        {
            vigilando = false;
            estadoActual = EstadoEnemigo.Patrullando;

            tiempoRestanteVigilancia = tiempoVigilancia;
        }
    }
    void ElegirDestinoAleatorio()
    {
        indicePosicionPatrulla = Random.Range(0, posicionPatrulla.Length-1);
        enemigo.SetDestination(posicionPatrulla[indicePosicionPatrulla].position);
    }
}
