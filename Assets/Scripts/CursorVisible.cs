using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorVisible : MonoBehaviour
{
    public PauseMenuManager pauseMenuManager;

    // Update is called once per frame
    void Update()
    {
        if (pauseMenuManager.pausaActivada)
        {
            Cursor.visible = true;
        }
    }
}
