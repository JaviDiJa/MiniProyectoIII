using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.AI;

public class AgressiveEnemies : MonoBehaviour
{
    [SerializeField] private float tiempoVigilancia;
    [SerializeField] private float rangoPersecucion;
    [SerializeField] private float rangoDeteccion;

    private float tiempoRestanteVigilancia;

    private bool personajeDetectado;

    private NavMeshAgent enemigo;
    private Animator animator;

    private GameObject jugador;

    private Vector3 ultimaPosicionConocida;

    private Vector3 primeraPosicionEnemigo;

    private Vector3 comprobarPersonaje;

    private int indicePosicionPatrulla;

    public EnemyExplosion enemyExplosion;

    //public LayerMask layerMask;

    public enum EstadoEnemigo
    {
        Guardia,
        Vigilando,
        Persiguiendo
    }

    public EstadoEnemigo estadoActual;

    // Start is called before the first frame update
    void Awake()
    {
        enemigo = GetComponent<NavMeshAgent>();

        animator = GetComponent<Animator>();

        jugador = GameObject.FindObjectOfType<CharacterController>().gameObject;

    }

    private void Start()
    {
        primeraPosicionEnemigo = enemigo.transform.position;

        ultimaPosicionConocida = jugador.transform.position;

        tiempoRestanteVigilancia = tiempoVigilancia;

        estadoActual = EstadoEnemigo.Guardia;
    }


    // Update is called once per frame
    void Update()
    {
        comprobarPersonaje = jugador.transform.position - transform.position;

        switch (estadoActual)
        {
            case EstadoEnemigo.Guardia:

                Guardia();

                break;

            case EstadoEnemigo.Persiguiendo:

                Persiguiendo();

                animator.SetBool("isPersiguiendo", true);

                break;

            //case EstadoEnemigo.Vigilando:

            //    Vigilando();

            //    animator.SetBool("isPersiguiendo", false);

            //    break;
        }

        RecibirDaņo();

        RaycastHit lookAt;

        if (Physics.Raycast(transform.position, comprobarPersonaje, out lookAt, rangoDeteccion))
        {
            if (lookAt.collider.gameObject != jugador)
            {
                personajeDetectado = false;
            }

            else
            {
                personajeDetectado = true;
                Persiguiendo();
            }
        }

        if (Vector3.Distance(transform.position, jugador.transform.position) < rangoPersecucion)
        {
            personajeDetectado = true;
        }

        if (personajeDetectado)
        {
            estadoActual = EstadoEnemigo.Persiguiendo;
        }

        if (personajeDetectado && Vector3.Distance(transform.position, jugador.transform.position) > rangoPersecucion)
        {
            estadoActual = EstadoEnemigo.Guardia;
        }

        if (!personajeDetectado)
        {
            estadoActual = EstadoEnemigo.Guardia;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 3)
        {
            personajeDetectado = true;
        }
    }

    //private void OnTriggerExit(Collider other)
    //{
    //    vigilando = true;

    //    Vigilando();
    //}

    void RecibirDaņo()
    {
        if (enemyExplosion.currentEnemyHealth != enemyExplosion.maxEnemyHealth)
        {
            Persiguiendo();
            Debug.Log("Funcionando");
        }
    }

    void Guardia()
    {
        if (Vector3.Distance(enemigo.transform.position, primeraPosicionEnemigo) < 1f)
        {
            animator.SetBool("isPersiguiendo", false);
        } 
        else
        {
            enemigo.SetDestination(primeraPosicionEnemigo);
            animator.SetBool("isPersiguiendo", true);
        }

    }

    void Persiguiendo()
    {
        enemigo.SetDestination(jugador.transform.position);
    }

    //void Vigilando()
    //{
    //    estadoActual = EstadoEnemigo.Vigilando;

    //    enemigo.SetDestination(ultimaPosicionConocida);

    //    if (tiempoRestanteVigilancia > 0)
    //    {
    //        tiempoRestanteVigilancia -= Time.deltaTime;

    //        vigilando = true;
    //    }
    //    else if (tiempoRestanteVigilancia <= 0)
    //    {
    //        vigilando = false;
    //        estadoActual = EstadoEnemigo.Guardia;

    //        tiempoRestanteVigilancia = tiempoVigilancia;
    //    }
    //}
}
