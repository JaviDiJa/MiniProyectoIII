using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargaNivel1 : MonoBehaviour
{
    private bool nivel1Cargado;

    private void Start()
    {
        nivel1Cargado = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!nivel1Cargado)
        {
            SceneLoader.Instance.LoadGameScene2();
            nivel1Cargado = true;
        }
    }
}
