using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargaNivel2 : MonoBehaviour
{
    public GameObject enemigosNivel2;

    private bool nivel2Cargado;

    private void Start()
    {
        enemigosNivel2.gameObject.SetActive(false);

        nivel2Cargado = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!nivel2Cargado && other.gameObject.layer == 6)
        {
            SceneLoader.Instance.LoadGameScene2();
            nivel2Cargado = true;

            enemigosNivel2.gameObject.SetActive(true);
        }
    }
}
