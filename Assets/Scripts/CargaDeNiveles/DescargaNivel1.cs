using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DescargaNivel1 : MonoBehaviour
{
    public GameObject puertaNivel1;

    public BloqueoPuertas bloqueoPuertas;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 6) 
        {
            SceneLoader.Instance.RemoveGameScene1();

            bloqueoPuertas.puertaDesbloqueada = false;
        }
    }
}
