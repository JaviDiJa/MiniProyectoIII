using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargaNivel3 : MonoBehaviour
{
    public GameObject enemigosNivel3;

    private bool nivel3Cargado;

    private void Start()
    {
        nivel3Cargado = false;

        enemigosNivel3.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!nivel3Cargado && other.gameObject.layer == 6)
        {
            SceneLoader.Instance.LoadGameScene3();
            nivel3Cargado = true;

            enemigosNivel3.gameObject.SetActive(true);
        }
    }
}
