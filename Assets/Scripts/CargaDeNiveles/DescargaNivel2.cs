using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DescargaNivel2 : MonoBehaviour
{
    public BloqueoPuertas bloqueoPuertas;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 6)
        {
            SceneLoader.Instance.RemoveGameScene2();

            bloqueoPuertas.puertaDesbloqueada = false;
        }
    }
}
