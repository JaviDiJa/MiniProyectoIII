using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JuegoFinalizado : MonoBehaviour
{
    public GameObject endMenu;

    private void Start()
    {
        endMenu.SetActive(false);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Time.timeScale = 0f;
            endMenu.SetActive(true);

            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
