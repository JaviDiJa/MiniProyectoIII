using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public void LoadGameScene_Button()
    {
        SceneLoader.Instance.LoadGameScene1();
        SceneLoader.Instance.LoadGameLogicScene();
        SceneLoader.Instance.RemoveMainMenu();
    }

    public void LoadCreditsScene_Button()
    {
        SceneLoader.Instance.LoadCreditsScene();
        SceneLoader.Instance.RemoveMainMenu();
    }
    public void Exit_Button()
    {
        Application.Quit();
    }
}
