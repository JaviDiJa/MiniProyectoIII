using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Disparador : MonoBehaviour
{
    public GameObject proyectilPrefab;
    public Transform inicioDisparo;

    [SerializeField] public float velocidadBala;
    [SerializeField] public float cadencia;
    [SerializeField] public float siguienteDisparo;

    [SerializeField] public int totalAmmo;
    [SerializeField] public int maxAmmo;
    [SerializeField] public int currentAmmo;

    [SerializeField] public float tiempoRecarga;
    [SerializeField] public float tiempoApuntado;

    [SerializeField] private Transform puntoApuntado;

    private bool estaRecargando;

    public CharacterMovement characterMovement;

    public AudioSource audioDisparo;
    public AudioClip sonidoDisparo;

    public GameObject efectoDisparo;

    //Vector3 posicionCamara = Camera.main.transform.position;
    Vector2 centroPantalla;

    Vector3 direccionDisparo;

    [SerializeField] private Animator animator;

    private void Start()
    {
        efectoDisparo.SetActive(false);

        currentAmmo = maxAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        centroPantalla = new Vector2(Screen.width / 2, Screen.height / 2);

        if (Input.GetMouseButton(0) && Time.time >= siguienteDisparo && currentAmmo > 0 && !estaRecargando && !characterMovement.isRunning)
        {
            Disparar();
            siguienteDisparo = Time.time + 1f / cadencia;
            currentAmmo--;
        }

        if (estaRecargando)
            return;

        if (currentAmmo <= 0 || Input.GetKeyDown(KeyCode.R) && currentAmmo < maxAmmo && !characterMovement.isRunning)
        {
            StartCoroutine(Recargar());
            return;
        }
        
        if (currentAmmo > maxAmmo)
        {
            currentAmmo = maxAmmo;
        }
    }

    void Disparar()
    {
        if(currentAmmo > 0)
        {
            audioDisparo.PlayOneShot(sonidoDisparo);

            EfectoDisparo();

            Ray ray = Camera.main.ScreenPointToRay(centroPantalla);

            int layerMask = ~LayerMask.GetMask("Enemigo");

            if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f, layerMask))
            {
                puntoApuntado.position = raycastHit.point;
            }

            Vector3 direccion = (puntoApuntado.position - inicioDisparo.position).normalized;

            GameObject proyectil = Instantiate(proyectilPrefab, inicioDisparo.position, Quaternion.LookRotation(direccion, Vector3.forward));

            Rigidbody rb = proyectil.GetComponent<Rigidbody>();
            rb.AddForce(direccion * velocidadBala, ForceMode.Impulse);
        }
    }

    IEnumerator Recargar()
    {
        if(totalAmmo > 0)
        {
            estaRecargando = true;

            yield return new WaitForSeconds(tiempoRecarga);
            totalAmmo = totalAmmo - maxAmmo;
            currentAmmo = maxAmmo;
            estaRecargando = false;
        }
    }

    void EfectoDisparo()
    {
        efectoDisparo.SetActive(true);
        efectoDisparo.GetComponent<ParticleSystem>().Play();

        Invoke("DesactivarDisparo", 0.1f);
    }

    void DesactivadDisparo()
    {
        efectoDisparo.GetComponent<ParticleSystem>().Stop();
        efectoDisparo.SetActive(false);
    }
}
