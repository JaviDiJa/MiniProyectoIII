using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DañoTrampa : MonoBehaviour
{
    [SerializeField] private float daño;

    public GameObject jugador;
    public DesactivarTrampa desactivarTrampa;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && !desactivarTrampa.fuegosApagados)
        {
            jugador.GetComponent<HealthSliderManager>().RecibirDaño(daño);
        }
    }
}
