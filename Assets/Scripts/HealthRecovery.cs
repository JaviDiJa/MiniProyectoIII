using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthRecovery : MonoBehaviour
{
    [SerializeField] private float curacion;

    public HealthSliderManager healthSliderManager;

    public ParticleSystem vidaPrefab;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && healthSliderManager.vidaActual != healthSliderManager.vidaMax)
        {
            healthSliderManager.vidaActual = healthSliderManager.vidaActual + curacion;

            ParticleSystem vida = Instantiate(vidaPrefab, transform.position, Quaternion.identity);
            Invoke("Recoger", 0.25f);
        }
    }

    void Recoger()
    {
        Destroy(gameObject);
    }
}
