using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzPuertasHangar : MonoBehaviour
{
    public BloqueoPuertasHangar bloqueoPuertasHangar;

    private Light light;

    private void Start()
    {
        light = GetComponentInChildren<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        if (bloqueoPuertasHangar.puertaPrincipalDesbloqueada)
        {
            light.color = Color.green;
        }
    }
}
