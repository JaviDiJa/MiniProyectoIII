using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbrirPuertasBloqueadas : MonoBehaviour
{
    [SerializeField] private GameObject puertaDer;
    [SerializeField] private GameObject puertaIzq;

    private Animator animatorDer;
    private Animator animatorIzq;

    public BloqueoPuertas bloqueoPuertas;

    private void Awake()
    {
        animatorDer = puertaDer.GetComponent<Animator>();
        animatorIzq = puertaIzq.GetComponent<Animator>();

        animatorDer.enabled = false;
        animatorIzq.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (bloqueoPuertas.puertaDesbloqueada)
        {
            if (other.tag == "Player" || other.tag == "Enemigo")
            {
                animatorDer.SetBool("IsClosed", false);
                animatorDer.enabled = true;

                animatorIzq.SetBool("IsClosed", false);
                animatorIzq.enabled = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (bloqueoPuertas.puertaDesbloqueada)
        {
            if (other.tag == "Player" || other.tag == "Enemigo")
            {
                animatorDer.SetBool("IsClosed", true);
                animatorIzq.SetBool("IsClosed", true);
            }
        }
    }
}
