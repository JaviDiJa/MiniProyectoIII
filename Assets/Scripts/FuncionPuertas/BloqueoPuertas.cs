using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloqueoPuertas : MonoBehaviour
{
    public GameObject puerta;
    public GameObject luz;
    public GameObject humo;

    public bool puertaDesbloqueada;

    // Start is called before the first frame update
    void Start()
    {
        puertaDesbloqueada = false;
        humo.SetActive(false);
    }

    private void Update()
    {
        LuzActivada();
    }

    private void OnMouseDown()
    {
        puertaDesbloqueada = true;
    }

    void LuzActivada()
    {
        if (puertaDesbloqueada)
        {
            luz.SetActive(false);
            humo.SetActive(true);
        }
        else if (!puertaDesbloqueada)
        {
            luz.SetActive(true);
            humo.SetActive(false);
        }
    }
}
