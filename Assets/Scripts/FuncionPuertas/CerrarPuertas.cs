using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CerrarPuertas : MonoBehaviour
{
    [SerializeField] private GameObject puertaDer;
    [SerializeField] private GameObject puertaIzq;

    private Animator animatorDer;
    private Animator animatorIzq;

    private void Awake()
    {
        animatorDer = puertaDer.GetComponent<Animator>();
        animatorIzq = puertaIzq.GetComponent<Animator>();

        animatorDer.enabled = false;
        animatorIzq.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            animatorDer.SetBool("IsClosed", true);
            animatorIzq.SetBool("IsClosed", true);
        }
    }
}
