using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzPuertas : MonoBehaviour
{
    public BloqueoPuertas bloqueoPuertas;

    private Light light;

    private void Start()
    {
        light = GetComponentInChildren<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        if (bloqueoPuertas.puertaDesbloqueada)
        {
            light.color = Color.green;
        }
    }
}
