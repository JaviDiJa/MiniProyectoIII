using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloqueoPuertasHangar : MonoBehaviour
{
    public GameObject puertaPrincipal;
    public GameObject puertaPasillo;
    public GameObject luz;
    public GameObject humo;

    public bool puertaPrincipalDesbloqueada;
    public bool puertaPasilloDesbloqueda;

    // Start is called before the first frame update
    void Start()
    {
        puertaPrincipalDesbloqueada = false;
        puertaPasilloDesbloqueda = false;
        humo.SetActive(false);
    }

    private void Update()
    {
        LuzActivada();
    }

    private void OnMouseDown()
    {
        DesbloquearPuertas();
    }

    void DesbloquearPuertas()
    {
        puertaPrincipalDesbloqueada = true;
        puertaPasilloDesbloqueda = true;
    }

    void LuzActivada()
    {
        if (puertaPrincipalDesbloqueada)
        {
            luz.SetActive(false);
            humo.SetActive(true);
        }
        else if (!puertaPrincipalDesbloqueada)
        {
            luz.SetActive(true);
            humo.SetActive(false);
        }
    }
}
