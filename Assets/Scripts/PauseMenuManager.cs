using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuManager : MonoBehaviour
{
    public GameObject pauseMenu;
    public GameObject optionsMenu;
    public GameObject crosshair;
    public GameObject deathMenu;

    public bool pausaActivada;
    public bool opcionesActivadas;

    public HealthSliderManager healthSliderManager;

    public void Start()
    {
        Time.timeScale = 1f;

        crosshair.gameObject.SetActive(true);
        pauseMenu.gameObject.SetActive(false);
        optionsMenu.gameObject.SetActive(false);
        deathMenu.gameObject.SetActive(false);

        pausaActivada= false;
        opcionesActivadas = false;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void Update()
    {
        PauseGame();

        if(healthSliderManager.vidaActual <= 0)
        {
            deathMenu.SetActive(true);
            Time.timeScale = 0f;

            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    public void ResumeGame()
    {
        pauseMenu.gameObject.SetActive(false);
        pausaActivada = false;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        crosshair.gameObject.SetActive(true);
        Time.timeScale = 1f;
    }

    public void LoadOptions()
    {
        pauseMenu.gameObject.SetActive(false);
        optionsMenu.gameObject.SetActive(true);
        Cursor.visible = true;

        opcionesActivadas = true;
    }

    public void RemoveMenuOptions()
    {
        pauseMenu.gameObject.SetActive(true);
        optionsMenu.gameObject.SetActive(false);

        opcionesActivadas = false;
    }

    public void PauseGame()
    {

        if (Input.GetKeyDown(KeyCode.Escape) && !pausaActivada)
        {
            pauseMenu.gameObject.SetActive(true);
            pausaActivada= true;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            crosshair.gameObject.SetActive(false);

            Time.timeScale = 0f;
        }

        else if (Input.GetKeyDown(KeyCode.Escape) && pausaActivada || Input.GetKeyDown(KeyCode.Escape) && opcionesActivadas)
        {
            pauseMenu.gameObject.SetActive(false);
            pausaActivada = false;

            optionsMenu.gameObject.SetActive(false);
            opcionesActivadas = false;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            crosshair.gameObject.SetActive(true);

            Time.timeScale = 1f;
        }
    }

    public void Restart()
    {
        SceneLoader.Instance.RemoveGameLogicScene();

        SceneLoader.Instance.LoadGameLogicScene();
        SceneLoader.Instance.LoadGameScene1();

        deathMenu.gameObject.SetActive(false);

        SceneLoader.Instance.RemoveGameScene1();
        SceneLoader.Instance.RemoveGameScene2();
        SceneLoader.Instance.RemoveGameScene3();

        Time.timeScale = 1f;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void QuitToMenu()
    {
        SceneLoader.Instance.LoadMainMenu();
        Cursor.visible = true;

        deathMenu.gameObject.SetActive(false);

        SceneLoader.Instance.RemoveGameLogicScene();
        SceneLoader.Instance.RemoveGameScene1();
        SceneLoader.Instance.RemoveGameScene2();
        SceneLoader.Instance.RemoveGameScene3();
    }
}
