using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitBulletPrefab : MonoBehaviour
{
    public ParticleSystem hitBullet;

    private void OnDestroy()
    {
        Instantiate(hitBullet, transform.position, Quaternion.identity);

        DestroyImmediate(hitBullet);
    }
}
