using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.Rendering;

public class EnemyExplosion : MonoBehaviour
{
    [SerializeField] private float da�o;
    [SerializeField] private float destroyDelay;

    [SerializeField] public int maxEnemyHealth;
    [SerializeField] public int currentEnemyHealth;

    private GameObject jugador;
    private NavMeshAgent enemigo;

    public GameObject explosionPrefab;

    public GameObject crosshair;
    public GameObject hitMarker;

    public AudioSource audioExplosion;
    public AudioClip sonidoExplosion;

    // Start is called before the first frame update
    void Awake()
    {
        enemigo = GetComponent<NavMeshAgent>();
        jugador = GameObject.FindGameObjectWithTag("Player");

        currentEnemyHealth = maxEnemyHealth;
    }

    private void Start()
    {
        hitMarker.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 3)
        {
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(enemigo.transform.position, jugador.transform.position) < 1.5f)
        {
            jugador.GetComponent<HealthSliderManager>().RecibirDa�o(da�o);
            gameObject.SetActive(false);

            Explotar();
        }

        if (currentEnemyHealth <= 0)
        {
            Muerte();
        }
    }

    public void TakeDamage(int damage)
    {
        currentEnemyHealth -= damage;
        StartCoroutine("ResetCrosshair");
    }

    public void Explotar()
    {
        audioExplosion.PlayOneShot(sonidoExplosion);
        GameObject explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        Destroy(explosion, 2f);

        Destroy(gameObject, destroyDelay);
    }

    public void Muerte()
    {
        GameObject explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        Destroy(explosion, 2f);

        hitMarker.gameObject.SetActive(false);
        crosshair.gameObject.SetActive(true);

        Destroy(gameObject, destroyDelay);
        
    }

    IEnumerator ResetCrosshair()
    {
        hitMarker.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.1f);

        hitMarker.gameObject.SetActive(false);
        crosshair.gameObject.SetActive(true);

        Debug.Log("Cruzetilla");
    }
}
