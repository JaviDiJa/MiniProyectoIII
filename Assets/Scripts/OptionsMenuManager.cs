using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMenuManager : MonoBehaviour
{
    public void Back_Button()
    {
        SceneLoader.Instance.LoadMainMenu();
        SceneLoader.Instance.RemoveCreditsScene();
    }
}
