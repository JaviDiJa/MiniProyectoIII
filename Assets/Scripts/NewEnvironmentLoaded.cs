using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewEnvironmentLoaded : MonoBehaviour
{
    // Start is called before the first frame update
   IEnumerator Start()
    {
        yield return new WaitForSeconds(2f);
        FindObjectOfType<LoadingScreenManager>().DeActivate();
    }
}
