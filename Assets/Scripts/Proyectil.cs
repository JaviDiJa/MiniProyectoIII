using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Proyectil : MonoBehaviour
{
    [SerializeField] private float velocidad;

    [SerializeField] public int damage;

    public ParticleSystem hitBulletPrefab;
    public ParticleSystem enemyBulletPrefab;

    public void Init()
    {
        GetComponent<Rigidbody>().velocity = transform.right * velocidad;
    }


    public void OnCollisionEnter(Collision collision)
    {
        ParticleSystem impacto = Instantiate(hitBulletPrefab, transform.position, Quaternion.identity);
        Destroy(impacto);

        EnemyExplosion enemyExplosion = collision.gameObject.GetComponent<EnemyExplosion>();

        if (enemyExplosion != null)
        { 
            enemyExplosion.TakeDamage(damage);

            ParticleSystem impactoEnemigo = Instantiate(enemyBulletPrefab, transform.position, Quaternion.identity);
            Destroy(impactoEnemigo);
        }

        Destroy(gameObject);
    }
}
