using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class HealthSliderManager : MonoBehaviour
{
    [SerializeField] private Slider barraVida;

    [SerializeField] public float vidaMax;
    [SerializeField] public float vidaActual;

    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
        vidaActual = vidaMax;

        barraVida.maxValue = vidaMax;
        barraVida.value = vidaActual;
    }

    // Update is called once per frame
    void Update()
    {
        barraVida.value = vidaActual;
    }

    private void UpdateVida(float cambio)
    {
        vidaActual += cambio;

        if (vidaActual > vidaMax)
        {
            vidaActual = vidaMax;
        }

        barraVida.value = vidaActual;
    }

    public void RecibirDa�o(float da�o)
    {
        UpdateVida(-da�o);
    }

    public void RecibirCuracion(float curacion)
    {
        UpdateVida(curacion);
    }
}
