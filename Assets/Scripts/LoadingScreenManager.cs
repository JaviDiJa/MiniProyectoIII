using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreenManager : MonoBehaviour
{
    public Image bg;
    public GameObject textObject;

    //FindObjectOfType<LoadingScreenManager>().Activate();

    private void Start()
    {

        DeActivate();
    }
    public  void Activate()
    {
        bg.gameObject.SetActive(true);
        textObject.SetActive(true);
    }

    public void DeActivate()
    {
        bg.gameObject.SetActive(false);
        textObject.SetActive(false);

    }
}
